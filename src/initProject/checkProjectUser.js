const mongoose = require('mongoose');
const User = require('../models/user');
const {databaseOptions} = require('../config/config');

async function checkProjectUser () {
  const defaultUserData = require('./defaultUserData');
  const connection = await mongoose.connect(process.env.MONGO_URI, databaseOptions);

  await User.remove({}).exec();

  User.register({
    email: defaultUserData.email
  }, defaultUserData.password, () => connection.disconnect());

}

checkProjectUser();

