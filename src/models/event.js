const mongoose = require('mongoose');

let EventSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true
  },
}, {
  versionKey: false
});

module.exports = mongoose.model('Event', EventSchema);