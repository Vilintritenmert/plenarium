const mongoose = require('mongoose');
const {jwtExpiresTime} = require('../config/config');
const jwt = require('jsonwebtoken');
const passportLocalMongoose = require('passport-local-mongoose');

let UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  }
}, {
  versionKey: false
});

UserSchema.plugin(passportLocalMongoose, {
  usernameField: 'email',
  hashField: 'hash',
});

/**
 * Create Token
 */
UserSchema.methods.createToken = function () {
  return jwt.sign({id: this._id}, process.env.JWTSECRET,
    {expiresIn: jwtExpiresTime});
};


module.exports = mongoose.model('User', UserSchema);