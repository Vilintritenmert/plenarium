const express = require('express');
const router = express.Router();
const passport = require('passport');

router.post(
  'login'
  ,[passport.authenticate('local', {session: false})]
  ,function (request, response) {
    const {user} = request;

    response.json({
      token: user.createToken()
    })
  });

module.exports = router;