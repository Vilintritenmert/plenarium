const express = require('express');
const router = express.Router();
const Event = require('../../models/event');
const passport = require('passport');

router.get('/events',
  [passport.authenticate('jwt', {session: false})],
  async function(request, response){
  // TODO: Add filter by date and pagination

    const {currentPage} = request.query;
    const countPerPage = 10;
    const dbQuery = {};

    const [events, totalCount] = await Promise.all([
      Event.find(dbQuery).skip(countPerPage * currentPage).limit(countPerPage).exec(),
      Event.count(dbQuery).exec()
    ]);

    response.json({
      events,
      totalCount
    })
});

router.put('/event/:id',
[passport.authenticate('jwt', {session: false})],
  async function(request, response){
    const {id} = request.params;

    const event = await Event.findOne({_id: id}).exec();

    if (!event) {
      next(Error('Event not found'));
    }

    response.json({

    })
});

