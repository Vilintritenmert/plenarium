const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const {appPort} = require('./config/config');
const app = express();
const {databaseOptions} = require('./config/config');
const passport = require('passport');
const jwtStrategy = require('passport-jwt').Strategy;
const extractJwt = require('passport-jwt').ExtractJwt;
const localStrategy = require('passport-local').Strategy,;

mongoose.connect(process.env.MONGO_URI, databaseOptions);

app.listen(appPort, () => console.log(`App start on port ${appPort}`))
  .on('error', console.error);

app.use(passport)
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({extended: true}))
  .use(bodyParser.json({type: 'application/json'}))
  .disable('x-powered-by');

/**
 * Token strategy
 */
passport.use(new jwtStrategy({
  secretOrKey: process.env.JWTSECRET,
  jwtFromRequest: extractJwt.fromHeader('token'),
}, function (jwtPayload, done) {
  User.findOne({
    _id: jwtPayload.id,
  }, done);
}));

/**
 * Local user
 */
passport.use(new localStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


module.exports = app;