describe('Login as user', function () {
  context('when logged in', function () {
    it('should return -1', function () {
      [1, 2, 3].indexOf(4).should.equal(-1);
    });
  });
  context('when login failed', function () {
    it('should return the index where the element first appears in the array',
      function () {
        [1, 2, 3].indexOf(3).should.equal(2);
      });
  });
});