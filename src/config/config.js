module.exports = {
  jwtExpiresTime: '7 days',
  appPort: 5001,
  databaseOptions: {
    useNewUrlParser: true
  }
};